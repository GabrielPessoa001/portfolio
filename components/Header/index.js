import { Container, Title } from './style';

function Header () {
  return (
    <Container>
      <Title>० ० ० ० ० Gabriel Pessoa - Portfólio ० ० ० ० ०</Title>
    </Container>
  )
}

export default Header;
