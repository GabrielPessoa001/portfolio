import { Container } from './style';

function Layout ({ children, className }) {
  return (
    <Container className={ className }>
      { children }
    </Container>
  )
}

export default Layout;
