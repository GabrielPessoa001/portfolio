import { Title } from './style';

function Style (props) {
  return (
    <Title>➬ { props.title }</Title>
  )
}

export default Style;
