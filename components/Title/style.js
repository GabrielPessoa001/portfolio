import styled from 'styled-components';

export const Title = styled.h2`
  color: #00e0ff;
  font-size: 40px;

  margin: 0 0 0 1%;
`;
