import { Container, Title } from './style';

function Footer () {
  return (
    <Container>
      <Title>० ० ० ० ० ० ☂ ० ० ० ० ० ०</Title>
    </Container>
  )
}

export default Footer;
