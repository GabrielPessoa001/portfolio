import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  height: 5vh;

  background-color: #00e0ff;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.h3`
  color: #ffffff;
  font-size: 20px;
`;
