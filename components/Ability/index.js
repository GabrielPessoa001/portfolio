import { Container, Title, Description } from './style';

function Ability ({ title, description, children }) {
  return (
    <Container>
      <Title>{ title }</Title>
      <Description>{ children }{ description }</Description>
    </Container>
  )
}

export default Ability;
