import styled from 'styled-components';

export const Container = styled.div`
  width: 98%;
  height: auto;
  overflow: auto;

  margin: 1%;
  padding: 1px 1%;

  border-radius: 4px;
  box-shadow: 3px 3px 3px 3px rgba(0, 0, 0, 0.4);

  background-color: #ffffff;
`;

export const Title = styled.h2`
  color: #15b7b9;
`;

export const Description = styled.h3`
  color: #15b7b9;
`;
