import Header from '../components/Header';
import Footer from '../components/Footer';

import Layout from '../components/Layout';

import Ability from '../components/Ability';
import Title from '../components/Title';

export default function Home() {
  return (
    <>
      <Header />

      <Layout className="LayoutDashboard">
        <Ability title="Ⓑ Ⓘ Ⓣ Ⓑ Ⓤ Ⓒ Ⓚ Ⓔ Ⓣ">
          <a target="_blank" href="https://bitbucket.org/GabrielPessoa001/">─ Minha conta ─ ─ ─</a>
        </Ability>

        <Title title="Maiores habilidades:" />

        <Ability title="♚ Ruby on Rails">
          <h4>
            ➝ Tenho experiência com a criação de sites utilizando
            o framework Rails, da lingugagem de programação Ruby. Através desse framework, 
            consigo criar seções de usuários através de autenticação, consigo criar um
            sistema de listagem, criação, edição e remoção de um produto. Consigo criar uma
            infinidade de sites apenas com tais possibilidades, e alguns de meus repositórios
            presentes no BitBucket relacionados à Ruby on Rails comprovam isso:
          </h4>

          <a target="_blank" href="https://bitbucket.org/GabrielPessoa001/bookstore-ruby-on-rails/src/master/">
            ─ Bookstore
          </a>
          <br></br>

          <a target="_blank" href="https://bitbucket.org/GabrielPessoa001/geek-list-ruby-on-rails/src/master/">
            ─ Geek list
          </a>
        </Ability>

        <Ability title="♛ React e NextJS">
          <h4>
            ➝ React e NextJS são alguns dos mais atuais frameworks utilizando Node e, com ambos, consigo
            gera sites tanto estáticos quanto dinâmicos, possibilitando a criação de login, CRUD de produtos etc.
            Consigo fazer o consumo de APIs e transmitir meus dados dentro do site, assim como mostrado em alguns
            projetos no meu BitBucket, como segue abaixo:
          </h4>

          <a target="_blank" href="https://bitbucket.org/GabrielPessoa001/letras-de-musicas-react/src/master/">
            ─ Busca de músicas online
          </a>
          <br></br>

          <a target="_blank" href="https://bitbucket.org/GabrielPessoa001/hades-game-info-next/src/master/">
            ─ Descrição do jogo Hades
          </a>
        </Ability>

        <Title title="Cursos concluídos ou em conclusão:" />

        <Ability>
          <p>
            <strong>Título: </strong>Técnico em Informática<br />
            <strong>Instituição: </strong>Instituto Federal de Ciência e Educação do Ceará (IFCE)
          </p>
        </Ability>

        <Ability>
          <p>
            <strong>Título: </strong>Informática Básica<br />
            <strong>Instituição: </strong>SENAP
          </p>
        </Ability>

        <Ability>
          <p>
            <strong>Título: </strong>Programação Moderna<br />
            <strong>Instituição: </strong>COD3R [Udemy]
          </p>
        </Ability>

        <Title title="Experiência profissional:" />

        <Ability>
          <p>
            <strong>Locação: </strong>Programação usando Ruby on Rails<br />
            <strong>Trabalho: </strong>Criação do site de gerencimanento de atendimentos
              para os funcionários da Instituição<br /><br />
            <strong>Instituição: </strong>Cedeca CE
          </p>
        </Ability>

        <Ability>
          <p>
            <strong>Locação: </strong>Programação usando Laravel<br />
            <strong>Trabalho: </strong>Ajuda na criação de alguns sites para o uso dos alunos
            e funcionários do IFCE, tais como Intranet, site de notícias e informações,
            SysRA, site para o uso dos alunos para compra de refeições e dos funcionários
            para gerenciar o dinheiro dessas compras e o mais recente, chamado Aquisições,
            site que gerencia as licitações do campus<br /><br />
            <strong>Instituição: </strong>Instituto Federal de Ciência e Educação do Ceará (IFCE)
          </p>
        </Ability>
      </Layout>

      <Footer />
    </>
  )
}
